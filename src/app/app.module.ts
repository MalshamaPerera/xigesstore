import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupLoginComponent } from './signup-login/signup-login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WelcomepageComponent } from './welcomepage/welcomepage.component';
import { ShopPageComponent } from './shop-page/shop-page.component';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', component: WelcomepageComponent },
  { path: 'signup_login', component: SignupLoginComponent },
  {path: 'welcomepage' , component: WelcomepageComponent},
  {path: 'shop-page' , component: ShopPageComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    SignupLoginComponent,
    WelcomepageComponent,
    ShopPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
   
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
