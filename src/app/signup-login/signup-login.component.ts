import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import {FormBuilder, FormGroup, FormControl} from '@angular/forms';
import { NgForm } from '@angular/forms';



@Component({
  selector: 'app-signup-login',
  templateUrl: './signup-login.component.html',
  styleUrls: ['./signup-login.component.css']
})

export class SignupLoginComponent implements OnInit {


  firstname:any[];
  lastname:any[];
  contact:any[];
  email:any[];
  jobTitle:any[];
  password:any[];
  confirm:any[];

  
  private url ="http://192.168.8.100:4000/signup";
  private url1="http://192.168.8.100:4000/login"
 
  constructor(private http: HttpClient) {}
  ngOnInit(): void {}
  
  onSubmit(data){
   
    this.http.post(this.url,data)
    .subscribe((result)=>{
      console.warn("result",result)
    })
    console.warn(data)

    
  }

  onClick(logData){
    this.http.post(this.url1,logData)
    .subscribe((resultSet)=>{
      console.warn("result",resultSet)
    })
    console.warn(logData)
  }

}

